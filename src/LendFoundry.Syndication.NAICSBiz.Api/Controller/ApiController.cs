﻿using System;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.NAICSBiz.Request;
using LendFoundry.Syndication.NAICSBiz.Response;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.NAICSBiz.Api.Controller
{
  
    /// <summary>
    ///  ApiController class
    /// </summary>
    [Route("/")]
    public class ApiController : ExtendedController
    {
        #region Public Constructors

        /// <summary>
        ///  Constructor
        /// </summary>
        /// <param name="service"></param>
        public ApiController(IBizApiService service)
        {
            Service = service;
        }

        #endregion Public Constructors

        #region Private Properties

        private IBizApiService Service { get; }

        #endregion Private Properties

        #region Public Methods

      
        /// <summary>
        /// SearchSICcode
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("/{entitytype}/{entityid}/naics/search")]
        [ProducesResponseType(typeof(IBizApiResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> SearchSICcode(string entityType, string entityId, [FromBody]BizApiRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await Task.Run(() => Service.SearchSICcode(entityType, entityId, request)));
            });
        }

        #endregion Public Methods
    }
}