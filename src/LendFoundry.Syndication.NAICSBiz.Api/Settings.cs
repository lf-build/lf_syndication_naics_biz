﻿using LendFoundry.Foundation.Services.Settings;
using System;

namespace LendFoundry.Syndication.NAICSBiz.Api
{
    /// <summary>
    /// Settings
    /// </summary>
    public class Settings
    {
        /// <summary>
        /// ServiceName
        /// </summary>
        /// <returns></returns>
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "naics";
    }
}