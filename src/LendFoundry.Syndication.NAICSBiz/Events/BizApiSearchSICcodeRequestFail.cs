﻿
using LendFoundry.SyndicationStore.Events;

namespace LendFoundry.Syndication.NAICSBiz.Events
{
    public class BizApiSearchSicCodeRequestFail : SyndicationCalledEvent
    {
    }
}