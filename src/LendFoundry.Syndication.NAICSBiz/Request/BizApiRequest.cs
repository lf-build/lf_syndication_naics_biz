﻿using System;

namespace LendFoundry.Syndication.NAICSBiz.Request
{
    public class BizApiRequest : IBizApiRequest
    {
        #region Public Properties

        public string CompanyName { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Postalcode { get; set; }
        #endregion Public Properties
    }
}