﻿using LendFoundry.Syndication.NAICSBiz.Response.Proxy;

namespace LendFoundry.Syndication.NAICSBiz.Proxy
{
    public interface IBizApiProxy
    {
        #region Public Methods

        IBizApiResponse SearchSICcode(BizApiRequest requestParam);

        #endregion Public Methods
    }
}