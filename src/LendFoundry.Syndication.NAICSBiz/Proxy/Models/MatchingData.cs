﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.NAICSBiz.Response.Proxy
{
    public class MatchingData: IMatchingData
    {
        [JsonProperty("BEMFAB")]
        public string BEMFAB { get; set; }
        [JsonProperty("Match Grade")]
        public string MatchGrade { get; set; }
        [JsonProperty("Confidence Code")]
        public string ConfidenceCode { get; set; }
        [JsonProperty("DUNS #")]
        public string DUNS { get; set; }
        [JsonProperty("Matches Remaining")]
        public string MatchesRemaining { get; set; }
    }
}
