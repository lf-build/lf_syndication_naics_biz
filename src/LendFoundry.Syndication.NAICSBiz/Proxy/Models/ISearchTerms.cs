﻿namespace LendFoundry.Syndication.NAICSBiz.Response.Proxy
{
    public interface ISearchTerms
    {
        string City { get; set; }
        string CompanyName { get; set; }
        string Country { get; set; }
        string Phone { get; set; }
        string State { get; set; }
        string StreetAddress { get; set; }
        string ZipCode { get; set; }
    }
}