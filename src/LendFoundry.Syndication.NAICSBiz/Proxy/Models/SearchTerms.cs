﻿

using Newtonsoft.Json;

namespace LendFoundry.Syndication.NAICSBiz.Response.Proxy
{
    public class SearchTerms:ISearchTerms
    {
        [JsonProperty("Company Name")]
        public string CompanyName { get; set; }
        [JsonProperty("Street Address")]
        public string StreetAddress { get; set; }
        [JsonProperty("City")]
        public string City { get; set; }
        [JsonProperty("State")]
        public string State { get; set; }
        [JsonProperty("Zip Code")]
        public string ZipCode { get; set; }
        [JsonProperty("Country")]
        public string Country { get; set; }
        [JsonProperty("Phone")]
        public string Phone { get; set; }
    }
}
