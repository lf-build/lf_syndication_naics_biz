﻿namespace LendFoundry.Syndication.NAICSBiz.Response.Proxy
{
    public interface IMatchingData
    {
        string BEMFAB { get; set; }
        string ConfidenceCode { get; set; }
        string DUNS { get; set; }
        string MatchesRemaining { get; set; }
        string MatchGrade { get; set; }
    }
}