﻿
using Newtonsoft.Json;

namespace LendFoundry.Syndication.NAICSBiz.Response.Proxy
{
    public class AppendedData: IAppendedData
    {
        [JsonProperty("Company Name")]
        public string CompanyName { get; set; }
        [JsonProperty("Secondary Business Name")]
        public string SecondaryBusinessName { get; set; }
        [JsonProperty("Address Source")]
        public string AddressSource { get; set; }
        [JsonProperty("Street Address")]
        public string StreetAddress { get; set; }
        [JsonProperty("PO Box")]
        public string POBox { get; set; }
        [JsonProperty("State/Province")]
        public string State_Province { get; set; }
        [JsonProperty("City")]
        public string City { get; set; }
        [JsonProperty("ZIP Code")]
        public string ZIPCode { get; set; }
        [JsonProperty("Phone")]
        public string Phone { get; set; }
        [JsonProperty("CEO Title")]
        public string CEOTitle { get; set; }
        [JsonProperty("CEO Name")]
        public string CEOName { get; set; }
        [JsonProperty("CEO First Name")]
        public string CEOFirstName { get; set; }
        [JsonProperty("CEO Last Name")]
        public string CEOLastName { get; set; }
        [JsonProperty("Line of Business")]
        public string LineofBusiness { get; set; }
        [JsonProperty("Location Type")]
        public string LocationType { get; set; }
        [JsonProperty("Year Started")]
        public string YearStarted { get; set; }
        [JsonProperty("Employees on Site")]
        public string EmployeesonSite { get; set; }
        [JsonProperty("Employees Total")]
        public string EmployeesTotal { get; set; }
        [JsonProperty("Sales Volume")]
        public string SalesVolume { get; set; }
        [JsonProperty("4 Digit SIC 1")]
        public string FourDigitSIC1 { get; set; }
        [JsonProperty("4 Digit SIC 1 Description")]
        public string FourDigitSIC1Description { get; set; }
        [JsonProperty("4 Digit SIC 2")]
        public string FourDigitSIC2 { get; set; }
        [JsonProperty("4 Digit SIC 2 Description")]
        public string FourDigitSIC2Description { get; set; }
        [JsonProperty("8 Digit SIC 1")]
        public string EightDigitSIC1 { get; set; }
        [JsonProperty("8 Digit SIC 1 Description")]
        public string EightDigitSIC1Description { get; set; }
        [JsonProperty("8 Digit SIC 2")]
        public string EightDigitSIC2 { get; set; }
        [JsonProperty("8 Digit SIC 2 Description")]
        public string EightDigitSIC2Description { get; set; }
        [JsonProperty("NAICS 1 Code")]
        public string NAICS_1Code { get; set; }
        [JsonProperty("NAICS 1 Description")]
        public string NAICS_1Description { get; set; }
        [JsonProperty("NAICS 2 Code")]
        public string NAICS_2Code { get; set; }
        [JsonProperty("NAICS 2 Description")]
        public string NAICS_2Description { get; set; }
    }
}
