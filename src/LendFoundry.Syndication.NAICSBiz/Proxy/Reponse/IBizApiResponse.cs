﻿namespace LendFoundry.Syndication.NAICSBiz.Response.Proxy
{
    public interface IBizApiResponse
    {
        #region Public Properties

         ISearchTerms SearchTerms { get; set; }
         IMatchingData MatchingData { get; set; }
         IAppendedData AppendedData { get; set; }

        #endregion Public Properties
    }
}