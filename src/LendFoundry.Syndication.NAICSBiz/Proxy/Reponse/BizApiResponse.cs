﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.NAICSBiz.Response.Proxy
{
    public class BizApiResponse : IBizApiResponse
    {
        #region Public Properties

        [JsonConverter(typeof(ConcreteJsonConverter<SearchTerms>))]
        [JsonProperty("Search Terms")]
        public ISearchTerms SearchTerms { get; set; }
        [JsonConverter(typeof(ConcreteJsonConverter<MatchingData>))]
        [JsonProperty("Matching Data")]
        public IMatchingData MatchingData { get; set; }
        [JsonConverter(typeof(ConcreteJsonConverter<AppendedData>))]
        [JsonProperty("Appended Data")]
        public IAppendedData AppendedData { get; set; }

        #endregion Public Properties
    }
}