﻿using Newtonsoft.Json;
using System;

namespace LendFoundry.Syndication.NAICSBiz.Proxy
{
    public class BizApiRequest : IBizApiRequest
    {
        #region Public Constructors

        public BizApiRequest()
        {
        }

        public BizApiRequest(Request.IBizApiRequest request)
        {
            CompanyName = request.CompanyName;
            City = request.City;
            State = request.State;
            Country = request.Country;
            Address = request.Address;
            Phone = request.Phone;
            Postalcode = request.Postalcode;
      }

        #endregion Public Constructors

        #region Public Properties

        [JsonProperty(PropertyName = "companyName")]
        public string CompanyName { get; set; }
        [JsonProperty(PropertyName = "city")]
        public string City { get; set; }
        [JsonProperty(PropertyName = "state")]
        public string State { get; set; }
        [JsonProperty(PropertyName = "country")]
        public string Country { get; set; }
        [JsonProperty(PropertyName = "address")]
        public string Address { get; set; }
        [JsonProperty(PropertyName = "phone")]
        public string Phone { get; set; }
        [JsonProperty(PropertyName = "postalCode")]
        public string Postalcode { get; set; }
        #endregion Public Properties
    }
}