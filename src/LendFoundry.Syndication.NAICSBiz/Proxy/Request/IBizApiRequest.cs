﻿using System;

namespace LendFoundry.Syndication.NAICSBiz.Proxy
{
    public interface IBizApiRequest
    {
        #region Public Properties
        string CompanyName { get; set; }
        string City { get; set; }
        string State { get; set; }
        string Country { get; set; }
        string Address { get; set; }
        string Phone { get; set; }
        string Postalcode { get; set; }
        #endregion Public Properties
    }
}