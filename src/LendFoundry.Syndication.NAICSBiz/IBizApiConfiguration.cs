﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;
namespace LendFoundry.Syndication.NAICSBiz
{
    public interface IBizApiConfiguration : IDependencyConfiguration
    {
        #region Public Properties

        string UserName { get; set; }
        string Password { get; set; }
        string BaseUrl { get; set; }
        bool UseProxy { get; set; }
        string ProxyUrl { get; set; }
        string ConnectionString { get; set; }

        #endregion Public Properties
    }
}