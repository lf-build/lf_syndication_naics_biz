﻿using LendFoundry.Syndication.NAICSBiz.Response.Models;
using System.Collections.Generic;

namespace LendFoundry.Syndication.NAICSBiz.Response
{
    public interface IBizApiResponse
    {
        #region Public Properties

         ISearchTerms SearchTerms { get; set; }
         IMatchingData MatchingData { get; set; }
         IAppendedData AppendedData { get; set; }

        #endregion Public Properties
    }
}