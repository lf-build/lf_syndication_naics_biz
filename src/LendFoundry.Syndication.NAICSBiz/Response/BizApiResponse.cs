﻿using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.NAICSBiz.Response.Models;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.NAICSBiz.Response
{
    public class BizApiResponse : IBizApiResponse
    {
        public BizApiResponse()
        {

        }
        public BizApiResponse(Proxy.IBizApiResponse bizApiResponse)
        {
            if (bizApiResponse!=null)
            {
                SearchTerms = new SearchTerms(bizApiResponse.SearchTerms);
                MatchingData = new MatchingData(bizApiResponse.MatchingData);
                AppendedData = new AppendedData(bizApiResponse.AppendedData);
            }
        }

        #region Public Properties

        [JsonConverter(typeof(InterfaceConverter<ISearchTerms, SearchTerms>))]
        public ISearchTerms SearchTerms { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IMatchingData, MatchingData>))]
        public IMatchingData MatchingData { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IAppendedData, AppendedData>))]
        public IAppendedData AppendedData { get; set; }

        #endregion Public Properties
    }
}