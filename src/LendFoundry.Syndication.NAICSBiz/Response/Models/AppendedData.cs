﻿namespace LendFoundry.Syndication.NAICSBiz.Response.Models
{
    public class AppendedData: IAppendedData
    {
        public AppendedData()
        {

        }
        public AppendedData(Proxy.IAppendedData appendData)
        {
            if(appendData!=null)
            {
                CompanyName = appendData.CompanyName;
                SecondaryBusinessName = appendData.SecondaryBusinessName;
                AddressSource = appendData.AddressSource;
                StreetAddress = appendData.StreetAddress;
                POBox = appendData.POBox;
                State_Province = appendData.State_Province;
                City = appendData.City;
                ZIPCode = appendData.ZIPCode;
                Phone = appendData.Phone;
                CEOTitle = appendData.CEOTitle;
                CEOName = appendData.CEOName;
                CEOFirstName = appendData.CEOFirstName;
                CEOLastName = appendData.CEOLastName;
                LineofBusiness = appendData.LineofBusiness;
                LocationType = appendData.LocationType;
                YearStarted = appendData.YearStarted;
                EmployeesonSite = appendData.EmployeesonSite;
                EmployeesTotal = appendData.EmployeesTotal;
                SalesVolume = appendData.SalesVolume;
                FourDigitSIC1 = appendData.FourDigitSIC1;
                FourDigitSIC1Description = appendData.FourDigitSIC1Description;
                FourDigitSIC2 = appendData.FourDigitSIC2;
                FourDigitSIC2Description = appendData.FourDigitSIC2Description;
                EightDigitSIC1 = appendData.EightDigitSIC1;
                EightDigitSIC1Description = appendData.EightDigitSIC1Description;
                EightDigitSIC2 = appendData.EightDigitSIC2;
                EightDigitSIC2Description = appendData.EightDigitSIC2Description;
                NAICS_1Code = appendData.NAICS_1Code;
                NAICS_1Description = appendData.NAICS_1Description;
                NAICS_2Code = appendData.NAICS_2Code;
                NAICS_2Description = appendData.NAICS_2Description;
            }
        }
        public string CompanyName { get; set; }
        public string SecondaryBusinessName { get; set; }
        public string AddressSource { get; set; }
        public string StreetAddress { get; set; }
        public string POBox { get; set; }
        public string State_Province { get; set; }
        public string City { get; set; }
        public string ZIPCode { get; set; }
        public string Phone { get; set; }
        public string CEOTitle { get; set; }
        public string CEOName { get; set; }
        public string CEOFirstName { get; set; }
        public string CEOLastName { get; set; }
        public string LineofBusiness { get; set; }
        public string LocationType { get; set; }
        public string YearStarted { get; set; }
        public string EmployeesonSite { get; set; }
        public string EmployeesTotal { get; set; }
        public string SalesVolume { get; set; }
        public string FourDigitSIC1 { get; set; }
        public string FourDigitSIC1Description { get; set; }
        public string FourDigitSIC2 { get; set; }
        public string FourDigitSIC2Description { get; set; }
        public string EightDigitSIC1 { get; set; }
        public string EightDigitSIC1Description { get; set; }
        public string EightDigitSIC2 { get; set; }
        public string EightDigitSIC2Description { get; set; }
        public string NAICS_1Code { get; set; }
        public string NAICS_1Description { get; set; }
        public string NAICS_2Code { get; set; }
        public string NAICS_2Description { get; set; }
    }
}
