﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.NAICSBiz.Response.Models
{
    public class MatchingData: IMatchingData
    {
        public MatchingData()
        {
                
        }
        public MatchingData(Proxy.IMatchingData matchingData)
        {
            if (matchingData!=null)
            {
                BEMFAB = matchingData.BEMFAB;
                MatchGrade = matchingData.MatchGrade;
                ConfidenceCode = matchingData.ConfidenceCode;
                DUNS = matchingData.DUNS;
                MatchesRemaining = matchingData.MatchesRemaining;
            }
        }
        public string BEMFAB { get; set; }
        public string MatchGrade { get; set; }
        public string ConfidenceCode { get; set; }
        public string DUNS { get; set; }
        public string MatchesRemaining { get; set; }
    }
}
