﻿namespace LendFoundry.Syndication.NAICSBiz.Response.Models
{
    public interface IAppendedData
    {
        string AddressSource { get; set; }
        string CEOFirstName { get; set; }
        string CEOLastName { get; set; }
        string CEOName { get; set; }
        string CEOTitle { get; set; }
        string City { get; set; }
        string CompanyName { get; set; }
        string EightDigitSIC1 { get; set; }
        string EightDigitSIC1Description { get; set; }
        string EightDigitSIC2 { get; set; }
        string EightDigitSIC2Description { get; set; }
        string EmployeesonSite { get; set; }
        string EmployeesTotal { get; set; }
        string FourDigitSIC1 { get; set; }
        string FourDigitSIC1Description { get; set; }
        string FourDigitSIC2 { get; set; }
        string FourDigitSIC2Description { get; set; }
        string LineofBusiness { get; set; }
        string LocationType { get; set; }
        string NAICS_1Code { get; set; }
        string NAICS_1Description { get; set; }
        string NAICS_2Code { get; set; }
        string NAICS_2Description { get; set; }
        string Phone { get; set; }
        string POBox { get; set; }
        string SalesVolume { get; set; }
        string SecondaryBusinessName { get; set; }
        string State_Province { get; set; }
        string StreetAddress { get; set; }
        string YearStarted { get; set; }
        string ZIPCode { get; set; }
    }
}