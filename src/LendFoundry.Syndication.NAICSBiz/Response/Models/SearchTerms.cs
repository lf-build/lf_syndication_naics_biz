﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.NAICSBiz.Response.Models
{
    public class SearchTerms:ISearchTerms
    {
        public SearchTerms()
        {

        }
        public SearchTerms(Proxy.ISearchTerms searchTerm)
        {
            if (searchTerm!=null)
            {
                CompanyName = searchTerm.CompanyName;
                StreetAddress = searchTerm.StreetAddress;
                City = searchTerm.City;
                State = searchTerm.State;
                ZipCode = searchTerm.ZipCode;
                Country = searchTerm.Country;
                Phone = searchTerm.Phone;
            }
        }
        public string CompanyName { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
    }
}
