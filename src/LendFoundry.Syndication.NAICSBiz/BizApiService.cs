﻿using LendFoundry.Syndication.NAICSBiz.Events;
using LendFoundry.Syndication.NAICSBiz.Proxy;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Services;

using System;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.NAICSBiz
{
    public class BizApiService : IBizApiService
    {
        #region Public Constructors

        public BizApiService(IBizApiProxy proxy, IEventHubClient eventHub, ILookupService lookup)
        {
            if (proxy == null)
                throw new ArgumentNullException(nameof(proxy));
            if (eventHub == null)
                throw new ArgumentNullException(nameof(eventHub));
            if (lookup == null)
                throw new ArgumentNullException(nameof(lookup));
            Proxy = proxy;
            EventHub = eventHub;
            Lookup = lookup;
        }

        #endregion Public Constructors

        #region Private Properties

        private IEventHubClient EventHub { get; }
        private ILookupService Lookup { get; }
        private IBizApiProxy Proxy { get; }

        #endregion Private Properties

        #region Public Methods

        private string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException(nameof(entityType));

            entityType = entityType.ToLower();
            var validEntityTypes = Lookup.GetLookupEntry("entityTypes", entityType);

            if (validEntityTypes == null || !validEntityTypes.Any())
                throw new InvalidArgumentException("Invalid Entity Type");

            return entityType;
        }

        public async Task<Response.IBizApiResponse> SearchSICcode(string entityType, string entityId, Request.IBizApiRequest requestParam)
        {
          
            entityType = EnsureEntityType(entityType);

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            if (requestParam == null)
                throw new ArgumentNullException(nameof(requestParam));

            if (string.IsNullOrWhiteSpace(requestParam.CompanyName))
                throw new ArgumentNullException(nameof(requestParam.CompanyName));
            if (string.IsNullOrWhiteSpace(requestParam.City))
                throw new ArgumentNullException(nameof(requestParam.City));
            if (string.IsNullOrWhiteSpace(requestParam.State))
                throw new ArgumentNullException(nameof(requestParam.State));
            if (string.IsNullOrWhiteSpace(requestParam.Country))
                throw new ArgumentNullException(nameof(requestParam.Country));
            try
            {
                var proxyRequest = new Proxy.BizApiRequest(requestParam);
                var response = Proxy.SearchSICcode(proxyRequest);
                var result = new Response.BizApiResponse(response);
                await EventHub.Publish(new BizApiSearchSicCodeRequested
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = result,
                    Request = requestParam,
                    ReferenceNumber = Guid.NewGuid().ToString("N")
                });
                return result;
            }
            catch (Exception exception)
            {
                await EventHub.Publish(new BizApiSearchSicCodeRequestFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = exception.Message,
                    Request = requestParam,
                    ReferenceNumber = null
                });
                throw ;
            }
        }

        #endregion Public Methods
    }
}