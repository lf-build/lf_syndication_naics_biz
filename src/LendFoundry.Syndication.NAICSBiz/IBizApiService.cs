﻿using LendFoundry.Syndication.NAICSBiz.Request;
using LendFoundry.Syndication.NAICSBiz.Response;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.NAICSBiz
{
    public interface IBizApiService
    {
        #region Public Methods

        Task<IBizApiResponse> SearchSICcode(string entityType, string entityId, IBizApiRequest requestParam);

        #endregion Public Methods
    }
}