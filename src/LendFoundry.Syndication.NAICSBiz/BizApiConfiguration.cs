﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;
namespace LendFoundry.Syndication.NAICSBiz
{
    /// <summary>
    /// BBBSearchConfiguration class
    /// </summary>
    public class BizApiConfiguration : IBizApiConfiguration, IDependencyConfiguration
    {
        #region Public Properties

        public string UserName { get; set; }
        public string Password { get; set; }
        public string BaseUrl { get; set; }
        public bool UseProxy { get; set; } = true;
        public string ProxyUrl { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }
        #endregion Public Properties
    }
}