﻿using System;
using System.Runtime.Serialization;

namespace LendFoundry.Syndication.NAICSBiz
{
    [Serializable]
    public class BizApiException : Exception
    {
        #region Public Constructors

        public BizApiException()
        {
        }

        public BizApiException(string message) : this(null, message, null)
        {
        }

        public BizApiException(string errorCode, string message) : this(errorCode, message, null)
        {
        }

        public BizApiException(string message, Exception innerException) : this(null, message, innerException)
        {
        }

        public BizApiException(string errorCode, string message, Exception innerException) : base(message, innerException)
        {
            ErrorCode = errorCode;
        }

        #endregion Public Constructors

        #region Protected Constructors

        protected BizApiException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        #endregion Protected Constructors

        #region Public Properties

        public string ErrorCode { get; set; }

        #endregion Public Properties
    }
}