﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using System;
using Microsoft.Extensions.DependencyInjection;

namespace LendFoundry.Syndication.NAICSBiz.Client
{
    public class BizApiServiceClientFactory : IBizApiServiceClientFactory
    {
        #region Public Constructors

        public BizApiServiceClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
            Uri = new UriBuilder("http", endpoint, port).Uri;

        }

        public BizApiServiceClientFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }


        #endregion Public Constructors

        #region Private Properties

        private string Endpoint { get; }
        private int Port { get; }
        private IServiceProvider Provider { get; }
        private Uri Uri { get; }

        #endregion Private Properties

        #region Public Methods

        [Obsolete("Need to use the overloaded with Uri")]
        public IBizApiService Create(ITokenReader reader)
        {
            var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(reader, logger).Get("naics");
            }
            var client = Provider.GetServiceClient(reader, uri);
            return new BizApiService(client);
        }

        #endregion Public Methods
    }
}
