﻿using LendFoundry.Security.Tokens;
using LendFoundry.Foundation.Client;
using System;
using Microsoft.Extensions.DependencyInjection;

namespace LendFoundry.Syndication.NAICSBiz.Client
{
    /// <summary>
    /// all Service injection
    /// </summary>
    public static class BizApiServiceClientExtensions
    {
        #region Public Methods

        /// <summary>
        ///
        /// </summary>
        /// <param name="services">services</param>
        /// <param name="endpoint">endpoint of service</param>
        /// <param name="port">port of service</param>
        /// <returns>Service</returns>
        public static IServiceCollection AddBizApiService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IBizApiServiceClientFactory>(p => new BizApiServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IBizApiServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
        public static IServiceCollection AddBizApiService(this IServiceCollection services, Uri uri)
        {
            services.AddTransient<IBizApiServiceClientFactory>(p => new BizApiServiceClientFactory(p, uri));
            services.AddTransient(p => p.GetService<IBizApiServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddBizApiService(this IServiceCollection services)
        {
            services.AddTransient<IBizApiServiceClientFactory>(p => new BizApiServiceClientFactory(p));
            services.AddTransient(p => p.GetService<IBizApiServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
        #endregion Public Methods
    }
}
