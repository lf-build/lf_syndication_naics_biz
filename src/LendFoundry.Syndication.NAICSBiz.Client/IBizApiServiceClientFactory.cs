﻿using LendFoundry.Security.Tokens;
using LendFoundry.Syndication.NAICSBiz;

namespace LendFoundry.Syndication.NAICSBiz.Client
{
    public interface IBizApiServiceClientFactory
    {
        #region Public Methods

        IBizApiService Create(ITokenReader reader);

        #endregion Public Methods
    }
}