﻿using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.NAICSBiz.Request;
using LendFoundry.Syndication.NAICSBiz.Response;
using RestSharp;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.NAICSBiz.Client
{

    public class BizApiService : IBizApiService
    {
        #region Public Constructors

        public BizApiService(IServiceClient client)
        {
            Client = client;
        }

        #endregion Public Constructors

        #region Private Properties

        private IServiceClient Client { get; }

        #endregion Private Properties

        #region Public Methods

        public async Task<IBizApiResponse> SearchSICcode(string entityType, string entityId, IBizApiRequest requestParam)
        {
            var request = new RestRequest("/{entitytype}/{entityid}/naics/search", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            request.AddJsonBody(requestParam);
            return await Client.ExecuteAsync<BizApiResponse>(request);
        }

        #endregion Public Methods
    }
}